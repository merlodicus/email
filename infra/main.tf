provider "aws" {
  region              = "${var.aws_region}"
  access_key          = "${var.aws_access_key}"
  secret_key          = "${var.aws_secret_key}"
  # allowed_account_ids = ["${var.aws_account_id}"]
  version             = "2.11"
}

terraform {
  backend "s3" {
    bucket  = "email-terraform"
    key     = "email-terraform.tfstate"
    encrypt = true
  }
}

# epTODO this may be a duplicate if it already exists
resource "aws_route53_zone" "elliotpryde" {
  name = "elliotpryde.com"
}

# SES Domain Identity
resource "aws_ses_domain_identity" "elliotpryde_ses_domain_identity" {
  domain = "elliotpryde.com"
}

resource "aws_ses_domain_mail_from" "elliotpryde_ses_domain_mail_from" {
  domain           = "${aws_ses_domain_identity.elliotpryde_ses_domain_identity.domain}"
  mail_from_domain = "bounce.${aws_ses_domain_identity.elliotpryde_ses_domain_identity.domain}"
}

# Route53 MX record
resource "aws_route53_record" "elliotpryde_ses_domain_mail_from_mx" {
  zone_id = "${aws_route53_zone.elliotpryde.id}"
  name    = "${aws_ses_domain_mail_from.elliotpryde_ses_domain_mail_from.mail_from_domain}"
  type    = "MX"
  ttl     = "600"
  records = ["10 feedback-smtp.${var.aws_region}.amazonses.com"]
}

# Route53 TXT record for SPF
resource "aws_route53_record" "elliotpryde_ses_domain_mail_from_txt" {
  zone_id = "${aws_route53_zone.elliotpryde.id}"
  name    = "${aws_ses_domain_mail_from.elliotpryde_ses_domain_mail_from.mail_from_domain}"
  type    = "TXT"
  ttl     = "600"
  records = ["v=spf1 include:amazonses.com -all"]
}
